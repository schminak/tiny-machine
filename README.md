tiny-machine: A Computer for the Tiny Machine Language
======================================================

This program is a virtual machine which executes programs
written in the Tiny Machine language, as defined by
[Kenneth C. Louden](http://www.cs.sjsu.edu/faculty/louden/)
in his book
[Compiler Construction *Principles and Practice*](http://www.cs.sjsu.edu/faculty/louden/cmptext/).

In addition to functioning as a basic virtual machine,
<em>tiny-machine</em> is also intended to facilitate:
<dl>
  <dt>Debugging</dt>
  <dd>
    <p>
      The need for a better debugging tool for TM was one of the
      main motivating factors for writing <em>tiny-machine</em>.
      As a result, much effort has gone into developing its debugging facilities.
    </p>
    <p>
      <a href="README.md#interactive-mode">Interactive Mode</a> has a
      number of features mirroring some of the basic functions provided by
      <a href="https://www.gnu.org/software/gdb/documentation/">GDB</a>.
      Of particular note is the ability to read and edit all memory locations,
      set breakpoints in Instruction Memory, execute programs in
      step-wise/breakpoint-wise increments, and to export and import the
      running programs internal state as a core-dump.
    </p>
  </dd>
  <dt>Benchmarking and Profiling</dt>
  <dd>
    <p>
      <em>tiny-machine</em> enumerates the
      instructions executed in a program per opcode.
      By assigning a weight to each opcode, a discrete and
      repeatable benchmark is given for timing program execution.
    </p>
  </dd>
  <dt>Test Automation</dt>
  <dd>
    <p>
      Test cases are easily implemented using <em>tiny-machine</em>.
      All prompts and messages from the program are directed to standard-error,
      allowing relevant results to be piped out to other programs for assessment.
    </p>
  </dd>
</dl>

<dl>
  <dt><h2 id="synopsis">SYNOPSIS</h2></dt>
  <dd><code>tm</code> [ <em>options</em> ] [ <em>TM-file</em> [ <em>TM-args</em> ... ]]<br>
    <p><em>TM-args</em> may be any integer.
      Values too large to store in a word of data memory will still be read, but will overflow.</p>
    <p>One TM source file, <em>TM-file</em>, may be read in from the shell.
      The structure of an acceptable TM program is described under
      <a href="README.md#tm-definition">TM DEFINITION</a>.</p></dd>
  <dt><h2 id="options">OPTIONS</h2></dt>
  <dd>
    <dl>
      <dt><h3 id="usage-and-information">Usage and Information</h3></dt>
      <dd>
	<dl>
	  <dt><code>-h</code>, <code>--help</code></dt>
	  <dd>Show a help message and exit the program.</dd>
	  <dt><code>-i</code>, <code>--interactive</code></dt>
	  <dd>Run in interactive mode.</dd>
	  <dt><code>-l</code>, <code>--load-core</code> <em>file</em></dt>
	  <dd>Load a previously dumped core from <em>file</em> into memory.</dd>
	  <dt><code>-q</code>, <code>--quiet</code></dt>
	  <dd>Limit printing of messages.</dd>
	  <dt><code>-r</code>, <code>--out-results</code></dt>
	  <dd>Make OUT instruction print to standard output.</dd>
	  <dt><code>-s</code>, <code>--out-steps</code></dt>
	  <dd>Print total instructions executed to standard output.</dd>
	  <dt><code>-t</code>, <code>--out-time</code></dt>
	  <dd>Print opcode-weighted time of program execution to standard output.</dd>
	  <dt><code>-w</code>, <code>--time-weights</code> <em>file</em></dt>
	  <dd>
	    Read <em>file</em> defining time spent per opcode executed for
	    opcode-weighted time calculation. Weights are written as an alist
	    of the form: ((OPCODE . WEIGHT) ...).
	  </dd>
	</dl>
      </dd>
      <dt><h3 id="memory-constraint-options">Memory Constraint Options</h3></dt>
      <dd>
	<dl>
	  <dt><code>--length-imem</code> <em>n</em></dt>
	  <dd>Number of words, <em>n</em>, allocated for instruction memory.</dd>
	  <dt><code>--length-dmem</code> <em>n</em></dt>
	  <dd>Number of words, <em>n</em>, allocated for data memory.</dd>
	  <dt><code>--length-regs</code> <em>n</em></dt>
	  <dd>Number of registers, <em>n</em>.</dd>
	  <dt><code>--width-dmem</code> <em>b</em></dt>
	  <dd>Number of bits, <em>b</em>, for a word in data memory.</dd>
	  <dt><code>--width-regs</code> <em>b</em></dt>
	  <dd>Number of bits, <em>b</em>, per register.</dd>
	  <dt><code>--pc-index</code> <em>i</em></dt>
	  <dd>Sets the register at index <em>i</em> as the program counter.</dd>
	</dl>
      </dd>
    </dl>
  </dd>
  <dt><h2 id="interactive-mode">INTERACTIVE MODE</h2></dt>
  <dd>
    <dl>
      <dt><h3 id="interactive-commands">Interactive Commands</h3></dt>
      <dd>
	<dl>
	  <dt><code>c</code>, <code>continue</code> [ <em>n</em> ]</dt>
	  <dd>
	    Continue program execution, skipping <em>n</em> breakpoints.
	    When not specified, <em>n</em> defaults to 0.
	  </dd>
	  <dt><code>s</code>, <code>step</code> [ <em>n</em> ]</dt>
	  <dd>
	    Execute up to <em>n</em> instructions, stop for breakpoints.
	    When not specified, <em>n</em> defaults to 1.
	  </dd>
	  <dt><code>p</code>, <code>print</code> <em>type</em></dt>
	  <dd>
	    Print all values relevant to <em>type</em>, where <em>type</em> can be:
	    <ul>
	      <li> <code>b</code> (all active breakpoints)</li>
	      <li> <code>r</code> (the contents of each register)</li>
	    </ul>
	  </dd>
	  <dt><code>p</code>, <code>print</code> <em>type</em> <em>a</em> <em>n</em></dt>
	  <dd>
	    Print values of the specified memory type from address
	    <em>a</em> to <em>n</em>, where <em>type</em> can be:
	    <ul>
	      <li> <code>r</code> (register memory)</li>
	      <li> <code>d</code> (data memory)</li>
	      <li> <code>i</code> (instruction memory)</li>
	    </ul>
	  </dd>
	  <dt><code>C</code>, <code>clear</code> <em>type</em></dt>
	  <dd>
	    Clear contents of selected types, where <em>type</em> can be:
	    <ul>
	      <li> <code>r</code> (clear register memory)</li>
	      <li> <code>d</code> (clear data memory)</li>
	      <li> <code>i</code> (clear instruction memory)</li>
	      <li> <code>i</code> (clear breakpoints)</li>
	      <li> <code>a</code> (clear all types)</li>
	    </ul>
	  </dd>
	  <dt><code>b</code>, <code>breakpoint</code> [ <em>n</em> ]</dt>
	  <dd>Add breakpoint at <em>n</em>. Show active values if <em>n</em> not given.</dd>
	  <dt><code>k</code>, <code>delete</code> <em>n</em></dt>
	  <dd>Delete breakpoint at <em>n</em>, if there is one.</dd>
	  <dt><code>S</code>, <code>set</code> <em>type</em> <em>a</em> <em>v</em></dt>
	  <dd>
	    Set address <em>a</em> of memory region <em>type</em>
	    to value <em>v</em>, where <em>type</em> may be:
	    <ul>
	      <li> <code>r</code> (register memory)</li>
	      <li> <code>d</code> (data memory)</li>
	    </ul>
	  </dd>
	  <dt><code>S</code>, <code>set</code> <code>b</code> <em>n</em></dt>
	  <dd>Alternate syntax to add a breakpoint at <em>n</em>.</dd>
	  <dt><code>S</code>, <code>set</code> <code>i</code></dt>
	  <dd>
	    Enters prompt where the user can submit a TM
	    instruction to be written into instruction memory.
	    Entries follow the same grammar as the
	    <a href="README.md#tm-line-grammar">
	      instruction lines
	    </a>
	    of a TM file.
	  </dd>
	  <dt><code>j</code>, <code>jump</code> <em>a</em></dt>
	  <dd>Set the value stored in the PC register to <em>a</em>.</dd>
	  <dt><code>l</code>, <code>load</code></dt>
	  <dd>
	    Prompts the user for the name of a TM
	    file to read into instruction memory.
	  </dd>
	  <dt><code>core</code></dt>
	  <dd>
	    Prompts the user for the name of a file to write a core-dump to.
	  </dd>
	  <dt><code>t</code>, <code>trace</code></dt>
	  <dd>Toggle the printing of an instruction trace.</dd>
	  <dt><code>h</code>, <code>help</code></dt>
	  <dd>Show a help message listing these commands.</dd>
	  <dt><code>q</code>, <code>quit</code></dt>
	  <dd>Quit out of TM.</dd>
	</dl>
      </dd>
      <dt><h3 id="example-interactive-session">Example Interactive Session</h3></dt>
      <dd>
	Add breakpoints at instruction memory addresses 3, 6, and 8:
	<pre>
<code>you@(TM)> b 6
you@(TM)> b 3
you@(TM)> b 8</code>
	</pre>
	Display current breakpoints:
	<pre>
<code>you@(TM)> b
Active breakpoints:
3
6
8</code>
	</pre>
	Set the value of data memory address 1 to 4:
	<pre>
<code>you@(TM)> S d 1 4</code>
	</pre>
	Continue program execution, skipping the first encountered breakpoint:
	<pre>
<code>you@(TM)> c 1
Hit breakpoint at address 3
Hit breakpoint at address 6</code>
	</pre>
	Show the state of the registers:
	<pre>
<code>you@(TM)> p r
| 0: 0 | 1: 0 | 2: 0 | 3: 0 | 4: 0 | 5: 0 | 6: 1023 | 7: 6 |</code>
	</pre>
	Remove the breakpoint at instruction address 8,
	then display the existing breakpoints:
	<pre>
<code>you@(TM)> k 8
you@(TM)> b
Active breakpoints:
3
6</code>
	</pre>
	Continue program execution, skipping no breakpoints:
	<pre>
<code>you@(TM)> c
24
HALT</code>
	</pre>
	Exit the simulation:
	<pre>
<code>you@(TM)> q
Exiting TM</code>
	</pre>
      </dd>
    </dl>
  </dd>
  <dt><h2 id="tm-definition">TM DEFINITION</h2></dt>
  <dd>
    <dl>
      <dt><h3 id="memory-regions">Memory Regions</h3></dt>
      <dd>
	<dl>
	  <dt>Instruction Memory</dt>
	  <dd>
	    <p>
	      Access in documentation is abbreviated as: <strong>IMEM</strong>[ <em>address</em> ].<br>
	      Region containing the executing TM program.
	      Instruction Memory cannot be modified by the executing program.
	    </p>
	  </dd>
	  <dt>Data Memory</dt>
	  <dd>
	    <p>
	      Access in documentation abbreviated as: <strong>DMEM</strong>[ <em>address</em> ].<br>
	      Where data is stored during program execution.
	      Data Memory can both be read and written to by the executing program.
	      All values of Data Memory are initialized to 0,
	      except for the initial address,
	      which is set to the address of the highest accessible word of Data Memory.
	    </p>
	  </dd>
	  <dt>Registers</dt>
	  <dd>
	    <p>
	      Access in documentation abbreviated as: <strong>REGS</strong>[ <em>address</em> ].<br>
	      For holding data being actively manipulated.
	      The program counter register, <strong>PC</strong>=<strong>REGS</strong>[ <em>pc</em> ],
	      points to the next instruction to be executed from Instruction Memory.
	      Jumping to a particular address in Instruction Memory is thus achieved
	      by writing the address to <strong>PC</strong>.<br>
	      The default value of <em>pc</em> is 7.
	    </p>
	  </dd>
	</dl>
      </dd>
      <dt><h3 id="tm-file-grammar">TM File Grammar</h3></dt>
      <dd>
	<dl>
	  <dt><h4 id="tm-line-grammar">TM Line Grammar</h4></dt>
	  <dd>
	    <p>
	      Empty lines and lines consisting of only
	      whitespace are marked as <em>empty</em>.
	      Lines starting with an asterisk as their first
	      non-whitespace character are marked as <em>comment</em>.
	      Any lines not identified as having a valid instruction,
	      but not fitting under <em>blank</em> or
	      <em>comment</em> are marked as <em>erroneous</em>.
	      On loading a file with TM source code,
	      any line marked <em>empty</em>, <em>comment</em>,
	      or <em>erroneous</em> is simply ignored.
	    </p>
	    <p>
	      Valid <em>instruction</em> lines take the form:
	    </p>
	    <pre> instruction-address : instruction EOL-comment </pre>
	    <p>
	      Where <em>instruction-address</em> is the address the
	      instruction will be placed at in Instruction Memory,
	      <em>instruction</em> is a Register-Memory or Register-Only instruction,
	      and <em>EOL-comment</em> (optional) is any text (assumed to be a comment)
	      placed after the instruction and before a newline.
	    </p>
	    <p>
	      Whitespace is only needed to delimit the <em>instruction</em> and
	      <em>EOL-comment</em> if the <em>EOL-comment</em> begins with a numeric character.
	      Otherwise, whitespace is entirely optional, with excess whitespace being ignored.
	    </p>
	  </dd>
	  <dt><h4 id="tm-instruction-grammar">TM Instruction Grammar</h4></dt>
	  <dd>
	    <p>
	      Valid Register-Only instructions have the form:
	    </p>
	    <pre> OPCODE r1 , r2 , r3  </pre>
	    <p>
	      Where <em>r1</em>, <em>r2</em>,
	      and <em>r3</em> are addresses of Registers.
	      Whitespace as a delimiter is allowed between
	      each non-whitespace token, but is unnecessary.
	    </p>
	    <p>
	      Valid Register-Memory instructions have the form:
	    </p>
	    <pre> OPCODE r1 , offset ( r2 ) </pre>
	    <p>
	      Where <em>r1</em> and <em>r2</em> are addresses of Registers,
	      while <em>offset</em> is an integer constant.
	      Whitespace as a delimiter is allowed between
	      each non-whitespace token, but is unnecessary.
	    </p>
	  </dd>
	</dl>
      </dd>
      <dt><h3 id="register-only-instructions">Register-Only Instructions</h3></dt>
      <dd>
	<table>
	  <tr>
	    <th>Code</th>
	    <th>Resulting Operation</th>
	  </tr>
	  <tr>
	    <td><strong>HALT</strong> <em>r1</em>, <em>r2</em>, <em>r3</em></td>
	    <td>Stops program execution. Returns control to the user.</td>
	  </tr>
	  <tr>
	    <td><strong>IN</strong> <em>r1</em>, <em>r2</em>, <em>r3</em></td>
	    <td>
	      Reads an integer from standard input and writes
	      it into <strong>REGS</strong>[ <em>r1</em> ].
	    </td>
	  </tr>
	  <tr>
	    <td><strong>OUT</strong> <em>r1</em>, <em>r2</em>, <em>r3</em></td>
	    <td>Writes <strong>REGS</strong>[ <em>r1</em> ] to standard output.</td>
	  </tr>
	  <tr>
	    <td><strong>ADD</strong> <em>r1</em>, <em>r2</em>, <em>r3</em></td>
	    <td>
	      <strong>REGS</strong>[ <em>r1</em> ]
	      = <strong>REGS</strong>[ <em>r2</em> ]
	      + <strong>REGS</strong>[ <em>r3</em> ]
	    </td>
	  </tr>
	  <tr>
	    <td><strong>SUB</strong> <em>r1</em>, <em>r2</em>, <em>r3</em></td>
	    <td>
	      <strong>REGS</strong>[ <em>r1</em> ]
	      = <strong>REGS</strong>[ <em>r2</em> ]
	      - <strong>REGS</strong>[ <em>r3</em> ]
	    </td>
	  </tr>
	  <tr>
	    <td><strong>MUL</strong> <em>r1</em>, <em>r2</em>, <em>r3</em></td>
	    <td>
	      <strong>REGS</strong>[ <em>r1</em> ]
	      = <strong>REGS</strong>[ <em>r2</em> ]
	      * <strong>REGS</strong>[ <em>r3</em> ]
	    </td>
	  </tr>
	  <tr>
	    <td><strong>DIV</strong> <em>r1</em>, <em>r2</em>, <em>r3</em></td>
	    <td>
	      <strong>REGS</strong>[ <em>r1</em> ]
	      = <strong>REGS</strong>[ <em>r2</em> ]
	      / <strong>REGS</strong>[ <em>r3</em> ]<br>
	      if ( <strong>REGS</strong>[ <em>r3</em> ] == 0 ):<br>
	      &nbsp;&nbsp;&nbsp;&nbsp;
	      an error will be thrown.
	    </td>
	  </tr>
	</table>
      </dd>
      <dt><h3 id="register-memory-instructions">Register-Memory Instructions</h3></dt>
      <dd>
	<em>address</em> = <em>offset</em> + <strong>REGS</strong>[ <em>r2</em> ]<br>
	<table>
	  <tr>
	    <th>Code</th>
	    <th>Resulting Operation</th>
	  </tr>
	  <tr>
	    <td><strong>LD</strong> <em>r1</em>, <em>offset</em> ( <em>r2</em> )</td>
	    <td>
	      <strong>REGS</strong>[ <em>r1</em> ]
	      = <strong>DMEM</strong>[ <em>address</em> ]<br>
	      Loads value from Data Memory at <em>address</em>
	      into Register <em>r1</em>.
	    </td>
	  </tr>
	  <tr>
	    <td><strong>LDA</strong> <em>r1</em>, <em>offset</em> ( <em>r2</em> )</td>
	    <td>
	      <strong>REGS</strong>[ <em>r1</em> ] = <em>address</em><br>
	      Loads <em>address</em> into Register <em>r1</em>.
	    </td>
	  </tr>
	  <tr>
	    <td><strong>LDC</strong> <em>r1</em>, <em>offset</em> ( <em>r2</em> )</td>
	    <td>
	      <strong>REGS</strong>[ <em>r1</em> ] = <em>offset</em><br>
	      Loads constant <em>offset</em> into Register <em>r1</em>.
	    </td>
	  </tr>
	  <tr>
	    <td><strong>ST</strong> <em>r1</em>, <em>offset</em> ( <em>r2</em> )</td>
	    <td>
	      <strong>DMEM</strong>[ <em>address</em> ]
	      = <strong>REGS</strong>[ <em>r1</em> ]<br>
	      Stores value in Register <em>r1</em> into
	      Data Memory at <em>address</em>.
	    </td>
	  </tr>
	  <tr>
	    <td><strong>JLT</strong> <em>r1</em>, <em>offset</em> ( <em>r2</em> )</td>
	    <td>
	      if ( <strong>REGS</strong>[ <em>r1</em> ] &lt; 0 ):<br>
	      &nbsp;&nbsp;&nbsp;&nbsp;
	      <strong>REGS</strong>[ <strong>PC</strong> ] = address
	    </td>
	  </tr>
	  <tr>
	    <td><strong>JLE</strong> <em>r1</em>, <em>offset</em> ( <em>r2</em> )</td>
	    <td>
	      if ( <strong>REGS</strong>[ <em>r1</em> ] &lt;= 0 ):<br>
	      &nbsp;&nbsp;&nbsp;&nbsp;
	      <strong>REGS</strong>[ <strong>PC</strong> ] = address
	    </td>
	  </tr>
	  <tr>
	    <td><strong>JGE</strong> <em>r1</em>, <em>offset</em> ( <em>r2</em> )</td>
	    <td>
	      if ( <strong>REGS</strong>[ <em>r1</em> ] &gt;= 0 ):<br>
	      &nbsp;&nbsp;&nbsp;&nbsp;
	      <strong>REGS</strong>[ <strong>PC</strong> ] = address
	    </td>
	  </tr>
	  <tr>
	    <td><strong>JGT</strong> <em>r1</em>, <em>offset</em> ( <em>r2</em> )</td>
	    <td>
	      if ( <strong>REGS</strong>[ <em>r1</em> ] &gt; 0 ):<br>
	      &nbsp;&nbsp;&nbsp;&nbsp;
	      <strong>REGS</strong>[ <strong>PC</strong> ] = address
	    </td>
	  </tr>
	  <tr>
	    <td><strong>JEQ</strong> <em>r1</em>, <em>offset</em> ( <em>r2</em> )</td>
	    <td>
	      if ( <strong>REGS</strong>[ <em>r1</em> ] == 0 ):<br>
	      &nbsp;&nbsp;&nbsp;&nbsp;
	      <strong>REGS</strong>[ <strong>PC</strong> ] = address
	    </td>
	  </tr>
	  <tr>
	    <td><strong>JNE</strong> <em>r1</em>, <em>offset</em> ( <em>r2</em> )</td>
	    <td>
	      if ( <strong>REGS</strong>[ <em>r1</em> ] != 0 ):<br>
	      &nbsp;&nbsp;&nbsp;&nbsp;
	      <strong>REGS</strong>[ <strong>PC</strong> ] = address
	    </td>
	  </tr>
	</table>
      </dd>
    </dl>
  </dd>
  <dt><h2 id="install">INSTALL</h2></dt>
  <dd>
    <p>
      Requires <a href="https://www.gnu.org/software/guile/"><em>Gnu Guile 2.2</em></a> or higher.
      This can be installed in Debian 10 and 11 with just:<br>
    </p>
    <code>sudo apt install guile</code><br><br>
    <p>
      For installation by use of the Makefile,
      <a href="https://www.gnu.org/software/make/"><em>Gnu Make</em></a> is required:<br>
    </p>
    <code>sudo apt install make</code><br><br>
    <dl>
      <dt><h3 id="install-less-operation">"Install-less" Operation</h3></dt>
      <dd>
	<p>
	  <em>tiny-machine.scm</em> is an executable Scheme script which invokes
	  Guile when ran as a command in the shell. With Guile installed, it can
	  be ran without further work. However, while not within a directory
	  defined by the shell executable <em>$PATH</em> variable, it must be
	  called prefixed by its path. For example:<br>
	</p>
	<code>user@box:~/$ Downloads/tiny-machine/tiny-machine.scm --help</code><br><br>
	<p>
	  If a easily movable executable is desired, running <code>make</code>
	  in the repositories root directory without any specified targets will
	  generate an executable bash script <em>tm</em>, which calls
	  <em>tiny-machine.scm</em> at its location as of <em>tm</em>'s
	  generation. Thus <em>tm</em> must be regenerated whenever the source
	  files are moved.
	</p>
	<p>
	  <em>tm</em> can be executed without specifying its path by placing it
	  in the users local binary folder (<em>~/bin</em> or <em>~/.local/bin</em>).
	  Running the following in the repositories root directory:<br>
	</p>
	<code>make install-less</code><br><br>
	<p>
	  will generate <em>tm</em> - if not already done -
	  before placing it in <em>~/.local/bin</em>.
	  <em>tm</em> will become available as soon as the user starts a
	  new shell session. Note that these directories aren't always
	  included in the user's <em>$PATH</em>, but that can be changed by
	  placing the following line in the users <em>~/.bashrc</em> file:<br>
	</p>
	<code>PATH=$PATH:~/bin:~/.local/bin</code><br><br>
	<p>
	  Uninstalling <em>tm</em>, if installed through this method,
	  just consists of running:<br>
	</p>
	<code>make uninstall-less</code><br><br>
	<p>
	  in the repositories root directory.
	</p>
      </dd>
      <dt><h3 id="local-installation">Local Installation</h3></dt>
      <dd>
	<p>
	  Full local installation can be carried out without super-user privileges.
	  In the repositories root directory, type the following to install
	  it for the current user only:<br>
	</p>
	<code>make install</code><br><br>
	<p>
	  The necessary files will be put under the users <em>~/.local/bin</em>
	  and <em>~/.local/lib</em> folders. As <em>~/.local/bin</em> is in the
	  user's <em>$PATH</em> variable, it is called without specifying its
	  path once a new shell has been entered. Note that the <em>bin</em>
	  directories may have to be added to the user's <em>$PATH</em>
	  through the method described above.
	</p>
	<p>
	  An example of program usage would then be:<br>
	</p>
	<code>alyssa@knight:~/alyssa-p-hackers-compiler/misc/tm-programs$ tm sqrt-iter.tm 42</code><br><br>
	<p>
	  To uninstall <em>tiny-machine</em>, run the following in the
	  repositories root directory:<br>
	</p>
	<code>make uninstall</code><br><br>
      </dd>
      <dt><h3 id="system-installation">System Installation</h3></dt>
      <dd>
	<p>
	  If multiple users on a given machine need to be able to run
	  <em>tiny-machine</em>, system installation may be easier to
	  manage from an administrative perspective. Run the following
	  in the repositories root directory with super-user privileges:<br>
	</p>
	<code>make global-install</code><br><br>
	<p>
	  The necessary files will be placed in <em>/usr/local/bin</em> and
	  under <em>/usr/share/guile/site/</em> (depending on Guile version),
	  and can be ran in the same manner as shown for local installation.
	  If installed as root, the user will also want to run:<br>
	</p>
	<code>make clean</code><br><br>
	<p>
	  with super-user privileges. This is as <em>global-tm</em> is
	  generated during installation, to which the standard user wouldn't
	  have write privileges.
	</p>
	<p>
	  System uninstallation, then, is done with:<br>
	</p>
	<code>make global-uninstall</code><br><br>
	<p>
	  with super-user privileges,
	  and while in the repositories root directory.
	</p>
      </dd>
    </dl>
  </dd>
</dl>
