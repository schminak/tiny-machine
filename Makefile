MODULE-ENTRY = tiny-machine.scm
SOURCES-DIR = tiny-machine/
MODULE-SOURCES = $(SOURCES-DIR)parse-tm.scm \
	$(SOURCES-DIR)memory-tools-tm.scm \
	$(SOURCES-DIR)fetch-execute-tm.scm \
	$(SOURCES-DIR)driver-tm-go.scm \
	$(SOURCES-DIR)driver-tm-interactive.scm \
	$(SOURCES-DIR)time-tm.scm

LOCAL-LIBDIR = ~/.local/lib/$(SOURCES-DIR)
GLOBAL-LIBDIR = $(shell guile -c "(display (%site-dir))")/
LOCAL-BINDIR = ~/.local/bin/
GLOBAL-BINDIR = /usr/local/bin/

tm: $(MODULE-ENTRY)
	printf '#!/usr/bin/bash\n\n' > tm;
	printf "$(shell pwd)/$(MODULE-ENTRY)" >> tm;
	printf ' $$@\n' >> tm;
	chmod +x tm;

install-less: tm $(MODULE-ENTRY) $(MODULE-SOURCES)
	install -m 755 -D local-tm $(LOCAL-BINDIR)tm

uninstall-less:
	rm -f $(LOCAL-BINDIR)tm

local-tm:
	printf '#!/usr/bin/bash\n\n' > local-tm;
	printf "$(LOCAL-LIBDIR)$(MODULE-ENTRY)" >> local-tm;
	printf ' $$@\n' >> local-tm;
	chmod +x local-tm;

install: local-tm $(MODULE-ENTRY) $(MODULE-SOURCES)
	install -m 755 -D local-tm $(LOCAL-BINDIR)tm
	install -d $(LOCAL-LIBDIR)$(SOURCES-DIR)
	install -m 755 $(MODULE-ENTRY) $(LOCAL-LIBDIR)
	install -m 755 $(MODULE-SOURCES) $(LOCAL-LIBDIR)$(SOURCES-DIR)

uninstall:
	rm -rf $(LOCAL-LIBDIR)
	rm -rf $(LOCAL-BINDIR)tm

global-tm:
	printf '#!/usr/bin/bash\n\n' > global-tm;
	printf "$(GLOBAL-LIBDIR)$(MODULE-ENTRY)" >> global-tm;
	printf ' $$@\n' >> global-tm;
	chmod +x global-tm;

global-install: global-tm $(MODULE-ENTRY) $(MODULE-SOURCES)
	install -m 755 -D global-tm $(GLOBAL-BINDIR)tm
	install -d $(GLOBAL-LIBDIR)$(SOURCES-DIR)
	install -m 755 $(MODULE-ENTRY) $(GLOBAL-LIBDIR)
	install -m 755 $(MODULE-SOURCES) $(GLOBAL-LIBDIR)$(SOURCES-DIR)

global-uninstall:
	rm -f $(GLOBAL-LIBDIR)$(MODULE-ENTRY)
	rm -rf $(GLOBAL-LIBDIR)$(SOURCES-DIR)
	rm -f $(GLOBAL-BINDIR)tm

clean:
	rm -f tm local-tm global-tm
