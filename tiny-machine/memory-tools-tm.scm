;; Copyright (C) 2021 Nathan Paul Schmidt
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see https://www.gnu.org/licenses/.

(define-module (tiny-machine memory-tools-tm)
  #:use-module (ice-9 match)
  #:export (make-register-memory make-instruction-memory make-data-memory))

(define (artificial-overflow x N)
  (- (bit-extract x 0 N)
     (if (logbit? (1- N) (bit-extract x 0 N))
	 (expt 2 N)
	 0)))

(define (make-memory memory-length init-value write-func error-type clear-func)
  (define memory-vec (make-vector memory-length init-value))
  (define (bound-error? address) (or (> 0 address) (<= memory-length address)))
  (define (read-memory address)
    (when (bound-error? address) (throw 'tm-error error-type address))
    (vector-ref memory-vec address))
  (define (write-memory address value)
    (when (bound-error? address) (throw 'tm-error error-type address))
    (vector-set! memory-vec address (write-func value)))
  (define (clear-memory)
    (let clear-memory-loop ((a 0))
      (when (< a memory-length)
	(write-memory a (clear-func a))
	(clear-memory-loop (1+ a)))))
  (clear-memory)
  (lambda memory-operation
    (match
     memory-operation
     (('get) memory-vec)
     (('read address) (read-memory address))
     (('write address value) (write-memory address value))
     (('clear) (clear-memory))
     (('length) memory-length)
     (('bound-error? address) (bound-error? address)))))
    
(define (make-instruction-memory memory-length)
  (define halt '((type . register-only) (opcode . HALT) (r1 . 0) (r2 . 0) (r3 . 0)))
  (define (imem-clear a) (cons (cons 'address a) halt))
  (make-memory memory-length halt values 'IMEM-ERR imem-clear))

(define (make-int-memory memory-length memory-width first-value)
  (define (int-clear a) (if (> a 0) 0 first-value))
  (make-memory memory-length
	       0
	       (lambda (v) (artificial-overflow (truncate v) memory-width))
	       'DMEM-ERR
	       int-clear))

(define (make-data-memory memory-length memory-width)
  (make-int-memory memory-length memory-width (1- memory-length)))

(define (make-register-memory memory-length memory-width)
  (make-int-memory memory-length memory-width 0))
