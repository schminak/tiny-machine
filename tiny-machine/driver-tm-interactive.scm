;; Copyright (C) 2021 Nathan Paul Schmidt
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see https://www.gnu.org/licenses/.

(define-module (tiny-machine driver-tm-interactive)
  #:use-module (tiny-machine time-tm)
  #:use-module (tiny-machine parse-tm)
  #:use-module (ice-9 peg)
  #:use-module (ice-9 ftw)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (ice-9 readline)
  #:export (tm-interactive))

(define-peg-string-patterns
  "line <- ( WS command WS ) / invalid
command <- trace / core / continue / step / print / clear / breakpoint
         / delete-breakpoint / watch / set / jump / load / help / quit
core <-- core-str
core-str < 'core'
continue <-- continue-str WS N?
continue-str < 'continue' / ( 'c' ! 'l' )
step <-- step-str WS N?
step-str < 'step' / ( 's' ! 'e' )
print <-- print-str WS ((( 'r' / 'd' / 'i' ) WS A delim-ws N ) / 'b' / 'r' )
print-str < 'print' / 'p'
clear <-- clear-str WS ( 'a' / 'r' / 'd' / 'i' / 'b' )
clear-str < 'clear' / 'C'
breakpoint <-- breakpoint-str WS N?
breakpoint-str < 'breakpoint' / 'b'
delete-breakpoint <-- delete-str WS N
delete-str < 'delete' / 'k'
watch <-- watch-str WS N
watch-str < 'watch' / 'w'
set <-- set-str WS ((( 'r' / 'd' ) WS A delim-ws V ) / ( 'b' WS N ) / 'i' )
set-str < 'set' / 'S'
jump <-- jump-str WS A
jump-str < 'jump' / 'j'
load <-- load-str
load-str < 'load' / 'l'
trace <-- trace-str
trace-str < 'trace' / 't'
help <-- help-str
help-str < 'help' / 'h'
quit <-- quit-str
quit-str < 'quit' / 'q'
WS < [ \t] *
delim-ws < [ \t]+
N <-- [0-9]+
A <-- [0-9]+
V <-- ('-' [1-9] [0-9]*) / [0-9]+
invalid <-- anything
anything < . *
")

(define stderr (current-error-port))
(set-readline-output-port! stderr)

(define (make-breakpoints)
  (define breakpoints (list))
  (define (dedupe-breakpoints!)
    (let dedupe-loop ((checked-bps (list)) (rest-bps breakpoints))
      (cond ((null? rest-bps) (set! breakpoints (sort checked-bps <)))
	    ((memv (car rest-bps) (cdr rest-bps))
	     (dedupe-loop checked-bps (cdr rest-bps)))
	    (else (dedupe-loop (cons (car rest-bps) checked-bps) (cdr rest-bps))))))
  (define (add-breakpoints! bps)
    (set! breakpoints (merge breakpoints (sort bps <) <))
    (dedupe-breakpoints!))
  (define (delete-breakpoints! bps)
    (set! breakpoints (filter (lambda (bp) (not (memv bp bps))) breakpoints)))
  (define (breakpoint? bp) (memv bp breakpoints))
  (lambda args
    (match
     args
     (('is-bp? bp) (breakpoint? bp))
     (('add bps ...) (add-breakpoints! bps))
     (('delete bps ...) (delete-breakpoints! bps))
     (('clear) (set! breakpoints (list)))
     (('return) breakpoints))))

(define (tm-interactive step-func timer IMEM DMEM REGS index-pc options)
  (define (interactive-help) (display "\
Interactive-mode commands:
<c, continue> [N=0]        Continue program execution, skipping N breakpoints.
<s, step> [N=1]            Execute up to N instructions, stop for breakpoints.
<p, print> <b,r>           Print breakpoints (b) or values in registers (r).
<p, print> <r,d,i> (A) (N) Print values of specified memory type from A to N.
<C, clear> <a,r,d,i,b>     Clear contents of selected memory/breaks, or all (a).
<b, breakpoint> [N]        Add breakpoint at N. Show active values if N not given.
<k, delete> (N)            Delete breakpoint at N, if there is one.
<S, set> <r/d> (A) (V)     Set address A of REGS or DMEM to value V.
<S, set> <b> (N)           Alternate syntax to add a breakpoint at N.
<S, set> <i>               Prompt for TM instruction for IMEM (same syntax as file).
<j, jump> (A)              Set the value in the PC register to A.
<l, load>                  Prompt for filename to read into IMEM.
core                       Prompt for filename to write core-dump to.
<t, trace>                 Toggle the printing of an instruction trace.
<h, help>                  Show this help message.
<q, quit>                  Quit out of TM.

Example session:

;;; Add breakpoints at instruction addresses 3, 6, and 8:
you@(TM)> b 6
you@(TM)> b 3
you@(TM)> b 8
;;; Display current breakpoints:
you@(TM)> b
Active breakpoints:
3
6
8
;;; Set DMEM address 1 to 4:
you@(TM)> S d 1 4
;;; Continue program execution,
;;; skipping the first encountered breakpoint:
you@(TM)> c 1
Hit breakpoint at address 3
Hit breakpoint at address 6
;;; Show the state of the registers:
you@(TM)> p r
| 0: 0 | 1: 0 | 2: 0 | 3: 0 | 4: 0 | 5: 0 | 6: 1023 | 7: 6 |
;;; Remove the breakpoint at instruction address 8,
;;; then display the existing breakpoints:
you@(TM)> k 8
you@(TM)> b
Active breakpoints:
3
6
;;; Continue program execution, skipping no breakpoints:
you@(TM)> c
24
HALT
;;; Exit the simulation:
you@(TM)> q
Exiting TM
"))
  (define (print-instruction instruction)
    (match
     (assq-ref instruction 'type)
     ('register-only (format #f "~5@a : ~4@a ~2@a, ~3@a,  ~1@a\n"
			     (assq-ref instruction 'address)
			     (assq-ref instruction 'opcode)
			     (assq-ref instruction 'r1)
			     (assq-ref instruction 'r2)
			     (assq-ref instruction 'r3)))
     ('register-memory (format #f "~5@a : ~4@a ~2@a, ~3@a ( ~1@a )\n"
			       (assq-ref instruction 'address)
			       (assq-ref instruction 'opcode)
			       (assq-ref instruction 'r1)
			       (assq-ref instruction 'offset)
			       (assq-ref instruction 'r2)))))
  (define count-ops (make-opcode-iterator))
  (define bps (make-breakpoints))
  (define print-trace #f)
  (define (at-bp?) (bps 'is-bp? (REGS 'read index-pc)))
  (define (regular-file? name) (eq? 'regular (stat:type (stat name))))
  (define prompt-str
    (if (getlogin) (string-append (getlogin) "@(TM)> ") "@(TM)> "))
  (define (read-loop)
    (define line-in (match-pattern line (readline prompt-str)))
    (define (exec-loop N bp-func else-func)
      (when (> N 0)
	(when print-trace
	  (display (print-instruction (IMEM 'read (REGS 'read index-pc))) stderr))
	(let ((ret (step-func)))
	  (count-ops 'increment ret)
	  (when (at-bp?)
	    (format stderr
		    "Hit breakpoint at address ~a~%"
		    (REGS 'read index-pc)))
	  (cond ((memv ret '(HALT IMEM-ERR DMEM-ERR ZERO-DIV))
		 (format stderr "~a~%" ret))
		((at-bp?) (exec-loop (bp-func N) bp-func else-func))
		(else (exec-loop (else-func N) bp-func else-func))))))
    (define (step-tm N) (exec-loop N (lambda (n) 0) (lambda (n) (1- n))))
    (define (continue-tm N)
      (exec-loop (1+ N) (lambda (n) (1- n)) (lambda (n) n)))
    (define (load-tm-file)
      (with-readline-completion-function
       (make-completion-function (scandir "./" regular-file?))
       (lambda ()
	 (let* ((filename (readline "Enter TM file: "))
		(avp (false-if-exception
		      (read-tm-file (car (string-tokenize filename))))))
	   (when avp
	     (for-each (lambda (i) (IMEM 'write (car i) (cdr i))) avp)
	     (format stderr "Read file ~a to IMEM~%" filename))
	   (unless avp (display "No file loaded.\n" stderr))))))
    (define (write-core-file)
      (with-readline-completion-function
       (make-completion-function (scandir "./" regular-file?))
       (lambda ()
	 (let* ((file-str (readline "Write core to file: "))
		(filename (false-if-exception (car (string-tokenize file-str)))))
	   (when filename
	     (format stderr "Writing core to ~a~%" filename)
	     (with-output-to-file filename
	       (lambda ()
		 (write (list (cons 'imem (IMEM 'get))
			      (cons 'dmem (DMEM 'get))
			      (cons 'regs (REGS 'get)))))))
	   (unless filename (display "Invalid file. Abort core-dump.\n" stderr))))))
    (define (set-instruction)
      (let* ((instruction-line (readline "Enter instruction: "))
	     (avps (tm-string->index-tm-pairs instruction-line)))
	(if (and avps (not (or (null? avps) (IMEM 'bound-error? (caar avps)))))
	    (IMEM 'write (caar avps) (cdar avps))
	    (begin (display "Error reading instruction\n" stderr)
		   (set-instruction)))))
    (when line-in
      (match
       (peg:tree line-in)
       ('continue (continue-tm 0))
       (('continue ('N N)) (continue-tm (string->number N)))
       ('step (step-tm 1))
       (('step ('N N)) (step-tm (string->number N)))
       (('print "r")
	(display "|" stderr)
	(let r-print-loop ((i 0))
	  (when (< i (REGS 'length))
	    (format stderr " ~a: ~a |" i (REGS 'read i))
	    (r-print-loop (1+ i))))
	(newline stderr))
       ((or ('print "b") 'breakpoint)
	(if (null? (bps 'return))
	    (display "No active breakpoints.\n" stderr)
	    (display "Active breakpoints:\n" stderr))
	(for-each (lambda (b) (display b stderr) (newline stderr))
		  (bps 'return)))
       (('print s ('A a) ('N n))
	(let ((A (string->number a))
	      (N (string->number n))
	      (MEM (match s ("i" IMEM) ("d" DMEM) ("r" REGS))))
	  (when (<= A N)
	    (let print-loop ((i A))
	      (unless (MEM 'bound-error? i)
		(match
		 s
		 ("i" (display (print-instruction (MEM 'read i)) stderr))
		 (_ (format stderr "~5@a : ~a ~%" i (MEM 'read i)))))
	      (when (< i N) (print-loop (1+ i)))))))
       (('clear "a") (IMEM 'clear) (DMEM 'clear) (REGS 'clear) (bps 'clear))
       (('clear s) ((match s ("i" IMEM) ("d" DMEM) ("r" REGS) ("b" bps)) 'clear))
       ((or ('breakpoint ('N n)) ('set "b" ('N n))) (bps 'add (string->number n)))
       (('delete-breakpoint ('N n)) (bps 'delete (string->number n)))
       (('set s ('A a) ('V v))
	(let ((A (string->number a))
	      (V (string->number v))
	      (MEM (match s ("d" DMEM) ("r" REGS))))
	  (if (MEM 'bound-error? A)
	      (display "Bound error\n" stderr)
	      (MEM 'write A V))))
       (('set "i") (set-instruction))
       (('jump ('A a))
	(let ((A (string->number a)))
	  (if (IMEM 'bound-error? A)
	      (display "Bound error\n" stderr)
	      (REGS 'write index-pc A))))
       ('load (load-tm-file))
       ('core (write-core-file))
       ('trace (set! print-trace (not print-trace))
	       (format stderr "Trace printing is now ~a.\n"
		       (if print-trace "on" "off")))
       ('help (interactive-help))
       ('quit (display "Exiting TM\n" stderr) (exit))
       (_ (write (peg:tree line-in)) (newline stderr)))
      (read-loop)))
  (read-loop))
