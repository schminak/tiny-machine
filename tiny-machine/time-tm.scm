;; Copyright (C) 2021 Nathan Paul Schmidt
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see https://www.gnu.org/licenses/.

(define-module (tiny-machine time-tm)
  #:use-module (tiny-machine parse-tm)
  #:use-module (ice-9 match)
  #:export (make-opcode-iterator))

(define (make-opcode-iterator)
  (define opcode-count
    (map (lambda (op) (cons op 0))
	 '(IN OUT ADD SUB MUL DIV HALT LDC LDA LD ST JEQ JNE JLT JLE JGT JGE)))

  (define (opcode-increment opcode)
    (when (tm-opcode? opcode)
      (assq-set! opcode-count opcode (1+ (cdr (assq opcode opcode-count))))))

  (define opcode-weight
    (map (lambda (op) (cons op 1))
	 '(IN OUT ADD SUB MUL DIV HALT LDC LDA LD ST JEQ JNE JLT JLE JGT JGE)))

  (define (adjust-weight new-weights)
    (for-each (lambda (weight)
		(assq-set! opcode-weight (car weight) (cdr weight)))
	      new-weights))

  (define (execution-time)
    (apply + (map (lambda (steps weight) (* (cdr steps) (cdr weight)))
		  opcode-count
		  opcode-weight)))

  (adjust-weight
   '((IN . 100) (OUT . 100) (HALT . 1)
     (ADD . 3) (SUB . 3) (MUL . 10) (DIV . 50)
     (LDC . 1) (LDA . 1) (LD  . 10) (ST  . 10)
     (JEQ . 3) (JNE . 3) (JLT .  3) (JLE .  3) (JGT . 3) (JGE . 3)))

  (lambda args
    (match
     args
     ('() (list opcode-count opcode-weight))
     (('display-time)
      (display "Steps Executed: "   (current-error-port))
      (display (apply + (map cdr opcode-count)) (current-error-port))
      (display "\nTime Executing: " (current-error-port))
      (display (execution-time)     (current-error-port))
      (newline                      (current-error-port)))
     (('weight weights) (adjust-weight weights))
     (('steps) (apply + (map cdr opcode-count)))
     (('steps opcode . opcodes)
      (apply + (map (lambda (op) (cdr (assq op opcode-count)))
		    (cons opcode opcodes))))
     (('time) (execution-time))
     (('increment opcode) (opcode-increment opcode))
     (_ #f))))
