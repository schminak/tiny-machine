;; Copyright (C) 2021 Nathan Paul Schmidt
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see https://www.gnu.org/licenses/.

(define-module (tiny-machine parse-tm)
  #:use-module (ice-9 peg)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 match)
  #:export (tm-string->index-tm-pairs read-tm-file tm-opcode?))

(define-peg-string-patterns
"program <- line * 
line <- WS ( instruction / comment / erroneous / empty )
instruction <-- address WS colon WS ( register-only / register-memory )
address <-- [0-9]+
colon < ':'
register-only <-- r-opcode WS r1 WS comma WS r2 WS
                  comma WS r3 WS EOL-comment NL
r-opcode <-- 'IN' / 'OUT' / 'ADD' / 'SUB' / 'MUL' / 'DIV' / 'HALT'
register-memory <-- m-opcode WS r1 WS comma WS offset WS
                    lparen WS r2 WS rparen WS EOL-comment NL
m-opcode <-- 'LDC' / 'LDA' / 'LD' / 'ST'
           / 'JEQ' / 'JNE' / 'JLT' / 'JLE' / 'JGT' / 'JGE'
lparen < '('
rparen < ')'
offset <-- '-' ? [0-9]+
r1 <-- [0-9]+
r2 <-- [0-9]+
r3 <-- [0-9]+
comma < ','
ASTR < '*'
WS < [ \t] *
NL < [\n]
comment <-- WS ASTR WS (! NL .) * NL
EOL-comment < (! NL .) *
empty < NL *
erroneous <-- (! NL .) + NL
")

(define (program->assocs p)
  (define (tm->assoc i)
    (match
     i
     (('instruction A (type . attributes))
      (append (list 'instruction (tm->assoc A) (cons 'type type))
	      (map tm->assoc attributes)))
     (((or 'address 'offset 'r1 'r2 'r3) A) (cons (car i) (string->number A)))
     (((or 'r-opcode 'm-opcode) op) (cons 'opcode (string->symbol op)))
     (_ i))) ; Allows non-instruction lines to pass through unchanged.
  (map tm->assoc p))

(define (tm-string->index-tm-pairs tm-string)
  (define (erroneous-tm-filter l)
    (match
     l
     (('instruction . attributes) #t)
     (('erroneous line)
      (format (current-error-port) "Parsed erroneous TM line:\n~a\n" line) #f)
     (_ #f)))
  (define (tm-assocs->index-tm-pairs p)
    (map (lambda (i) (cons (cdr (assq 'address (cdr i))) (cdr i)))
	 (filter erroneous-tm-filter p)))
  (false-if-exception
   (tm-assocs->index-tm-pairs
    (program->assocs
     ;; PEG parsers compress lists of one object down to the object.
     ;; The following ensures tm-strings of one instruction aren't mishandled.
     (let ((p-tree (peg:tree (match-pattern program (format #f "~a~%" tm-string)))))
       (if (and p-tree (eq? 'instruction (car p-tree))) (list p-tree) p-tree))))))

(define (read-tm-file filename)
  (if (file-exists? filename)
      (tm-string->index-tm-pairs (call-with-input-file filename get-string-all))
      #f))

(define (tm-opcode? o)
  (memq o '(IN OUT ADD SUB MUL DIV HALT LDC LDA LD ST JEQ JNE JLT JLE JGT JGE)))
