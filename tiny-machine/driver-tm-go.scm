;; Copyright (C) 2021 Nathan Paul Schmidt
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see https://www.gnu.org/licenses/.

(define-module (tiny-machine driver-tm-go)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:export (tm-go))

(define (tm-go step-func timer IMEM DMEM REGS index-pc options)
  (define (print-exit msg)
    (unless (assq 'quiet options)
      (format (current-error-port) "Exit on ~a\n" msg)))
  (let tm-go-loop ((step-result (step-func)))
    (match
     step-result
     ('HALT (timer 'increment 'HALT) (print-exit "HALT"))
     (('IMEM-ERR . err-list) (print-exit "IMEM-ERR"))
     (('DMEM-ERR . err-list) (print-exit "DMEM-ERR"))
     (('ZERO-DIV . err-list) (print-exit "ZERO-DIV"))
     (_ (timer 'increment step-result) (tm-go-loop (step-func)))))
  (unless (assq 'quiet options) (timer 'display-time)))
