;; Copyright (C) 2021 Nathan Paul Schmidt
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see https://www.gnu.org/licenses/.

(define-module (tiny-machine fetch-execute-tm)
  #:use-module (ice-9 match)
  #:use-module (ice-9 readline)
  #:export (fetch-execute-cycle))

(define (fetch-execute-cycle imem dmem regs PC options)
  (define stderr (current-error-port))
  (define instruction (imem 'read (regs 'read PC)))
  (define (i attribute-sym) (cdr (assq attribute-sym instruction)))
  (define (R1) (i 'r1))
  (define (R2) (i 'r2))
  (define (R3) (i 'r3))
  (define (OFFSET) (i 'offset))
  (define (ADDRESS) (+ (regs 'read (R2)) (i 'offset)))
  (set-readline-output-port! stderr)
  (regs 'write PC (1+ (regs 'read PC)))
  (match
   (i 'opcode)
   ('IN
    (let read-int-loop
	((read-int (false-if-exception (string->number (readline "INPUT: ")))))
      (if read-int
	  (regs 'write (R1) read-int)
	  (read-int-loop
	   (false-if-exception (string->number (readline "TRY AGAIN: ")))))))
   ('OUT
    (cond ((assq 'out-results options) (display (regs 'read (R1))) (newline))
	  ((assq 'quiet options) 'OUT)
	  (else (display (regs 'read (R1)) stderr) (newline stderr))))
   ('ADD (regs 'write (R1) (+ (regs 'read (R2)) (regs 'read (R3)))))
   ('SUB (regs 'write (R1) (- (regs 'read (R2)) (regs 'read (R3)))))
   ('MUL (regs 'write (R1) (* (regs 'read (R2)) (regs 'read (R3)))))
   ('DIV (when (= 0 (regs 'read (R3))) (throw 'tm-error 'ZERO-DIV instruction))
	 (regs 'write (R1) (/ (regs 'read (R2)) (regs 'read (R3)))))
   ('HALT 'HALT)
   ('LDC (regs 'write (R1) (OFFSET)))
   ('LDA (regs 'write (R1) (ADDRESS)))
   ('LD  (regs 'write (R1) (dmem 'read (ADDRESS))))
   ('ST  (dmem 'write (ADDRESS) (regs 'read (R1))))
   ('JEQ (when   (=  (regs 'read (R1)) 0) (regs 'write PC (ADDRESS))))
   ('JNE (unless (=  (regs 'read (R1)) 0) (regs 'write PC (ADDRESS))))
   ('JLT (when   (<  (regs 'read (R1)) 0) (regs 'write PC (ADDRESS))))
   ('JLE (when   (<= (regs 'read (R1)) 0) (regs 'write PC (ADDRESS))))
   ('JGT (when   (>  (regs 'read (R1)) 0) (regs 'write PC (ADDRESS))))
   ('JGE (when   (>= (regs 'read (R1)) 0) (regs 'write PC (ADDRESS)))))
  (i 'opcode))
