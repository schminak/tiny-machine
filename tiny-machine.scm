#!/usr/bin/guile \
-e tiny-machine -s
!#

;; tiny-machine: a computer for the Tiny Machine language
;; as defined by Kenneth C. Louden
;;
;; Copyright (C) 2021 Nathan Paul Schmidt
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see https://www.gnu.org/licenses/.

(eval-when (expand load eval)
	   (let ((cf (current-filename)))
	     (when cf (add-to-load-path (dirname cf)))))

;; Allows calling tiny-machine through the shell, i.e. '-e tiny-machine -s'
(define (tiny-machine . args)
  (apply (module-ref (resolve-module '(tiny-machine)) 'tiny-machine) args))

(define-module (tiny-machine)
  #:use-module (tiny-machine parse-tm)
  #:use-module (tiny-machine memory-tools-tm)
  #:use-module (tiny-machine fetch-execute-tm)
  #:use-module (tiny-machine driver-tm-go)
  #:use-module (tiny-machine driver-tm-interactive)
  #:use-module (tiny-machine time-tm)
  #:use-module (ice-9 match)
  #:use-module (ice-9 getopt-long)
  #:use-module (srfi srfi-43)
  #:export (tiny-machine))

(define (tiny-machine shell-args)
  (define (display-help)
    (display "\
The Tiny Machine Emulator

USAGE

  tm [options] [TM_FILE [CLI_ARG ...]]

OPTIONS

  Program usage and information: 
  -h, --help              Show this help message and exit.

  -i, --interactive       Run TM interactively.

  -l, --load-core file    Load a previously dumped core from file into memory.

  -q, --quiet             Limit printing of messages.
  -r, --out-results       Make OUT instruction print to standard output.
  -s, --out-steps         Print total instructions executed to standard output.
  -t, --out-time          Print opcode-weighted time to standard output.

  -w, --time-weights file Read file defining time spent per opcode executed for
                          opcode-weighted time calculation. Weights are written
                          as an alist of the form: ((OPCODE . WEIGHT) ...).

  Modifying the default memory constraints:
  --length-imem n         Number of words, n, allocated for instruction memory.
  --length-dmem n         Number of words, n, allocated for data memory.
  --length-regs n         Number of registers, n.

  --width-dmem b          Number of bits, b, for a word in data memory.
  --width-regs b          Number of bits, b, for a register.

  --pc-index i            Sets the register at index i as the program counter.
"))
  (define (str->num? s) (string-every char-set:digit s))
  (define (str->num-op opts key def) (string->number (option-ref opts key def)))
  (define (read-file n) (and n (file-exists? n) (with-input-from-file n read)))
  (define (core-length region core)
    (if core (let ((c (assq region core))) (if c (vector-length (cdr c)) 0)) 0))
  (let* ((option-spec
	  `((help         (value #f) (single-char #\h))
	    (interactive  (value #f) (single-char #\i))
	    (load-core    (value #t) (single-char #\l))
	    (quiet        (value #f) (single-char #\q))
	    (out-results  (value #f) (single-char #\r))
	    (out-steps    (value #f) (single-char #\s))
	    (out-time     (value #f) (single-char #\t))
	    (time-weights (value #t) (single-char #\w))
	    (length-imem  (value #t) (predicate ,str->num?))
	    (length-dmem  (value #t) (predicate ,str->num?))
	    (length-regs  (value #t) (predicate ,str->num?))
	    (width-dmem   (value #t) (predicate ,str->num?))
	    (width-regs   (value #t) (predicate ,str->num?))
	    (pc-index     (value #t) (predicate ,str->num?))))
	 (options (getopt-long shell-args option-spec))
	 (weights-file (option-ref options 'time-weights #f))
	 (core-name    (option-ref options    'load-core #f))
	 (core-data (read-file core-name))
	 (ic-length (core-length 'imem core-data))
	 (dc-length (core-length 'dmem core-data))
	 (rc-length (core-length 'regs core-data))
	 (imem-length (max ic-length (str->num-op options 'length-imem "1024")))
	 (dmem-length (max dc-length (str->num-op options 'length-dmem "1024")))
	 (regs-length (max rc-length (str->num-op options 'length-regs    "8")))
	 (dmem-width (str->num-op options 'width-dmem "64"))
	 (regs-width (str->num-op options 'width-regs "64"))
	 (index-pc   (str->num-op options   'pc-index  "7"))
	 (time-weights (read-file weights-file))
	 (IMEM (make-instruction-memory imem-length))
	 (DMEM (make-data-memory dmem-length dmem-width))
	 (REGS (make-register-memory regs-length regs-width))
	 (timer (make-opcode-iterator))
	 (non-option-args (option-ref options '() #f))
	 (input-tm-file (and (not (null? non-option-args))
			     (read-tm-file (car non-option-args)))))
    (define (step-tm)
      (catch 'tm-error
	     (lambda () (fetch-execute-cycle IMEM DMEM REGS index-pc options))
	     (lambda (key . args) args)))

    (when (assq 'help options) (display-help) (exit))

    (when (or (> ic-length 0) (> dc-length 0) (> rc-length 0))
      (vector-copy! (IMEM 'get) 0 (cdr (assq 'imem core-data)))
      (vector-copy! (DMEM 'get) 0 (cdr (assq 'dmem core-data)))
      (vector-copy! (REGS 'get) 0 (cdr (assq 'regs core-data))))

    (unless (null? non-option-args)
      (let write-args-to-DMEM
	  ((argc 1)
	   (argv (cdr non-option-args)))
	(unless (or (null? argv) (> argc (1- (DMEM 'length))))
	  (let ((number-in (false-if-exception (string->number (car argv)))))
	    (if (and number-in (exact-integer? number-in))
		(begin (DMEM 'write argc number-in)
		       (write-args-to-DMEM (1+ argc) (cdr argv)))
		(write-args-to-DMEM argc (cdr argv)))))))

    (when input-tm-file
      (for-each (lambda (i) (IMEM 'write (car i) (cdr i))) input-tm-file))

    (when time-weights (timer 'weight time-weights))

    (if (or (assq 'interactive options) (not input-tm-file))
	(tm-interactive step-tm timer IMEM DMEM REGS index-pc options)
	(begin (tm-go step-tm timer IMEM DMEM REGS index-pc options)
	       (when (assq 'out-steps options) (display (timer 'steps)) (newline))
	       (when (assq  'out-time options) (display (timer  'time)) (newline))))))
